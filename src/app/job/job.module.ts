import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobRoutingModule } from './job-routing.module';
import { MainComponent } from './components/main/main.component';
import { SharedModule } from '../shared/shared.module';
import { JobService } from './services/job.service';
import { JobListComponent } from './components/job-list/job-list.component';
import { JobListItemComponent } from './components/job-list-item/job-list-item.component';
import { FavoriteJobListComponent } from './components/favorite-job-list/favorite-job-list.component';
import { SingleJobComponent } from './components/single-job/single-job.component';

@NgModule({
  declarations: [
    MainComponent,
    JobListComponent,
    JobListItemComponent,
    FavoriteJobListComponent,
    SingleJobComponent
  ],
  imports: [
    CommonModule,
    JobRoutingModule,
    SharedModule
  ],
  providers : [
    JobService
  ]
})
export class JobModule { }
