import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { SingleJobComponent } from './components/single-job/single-job.component';
import { JobListComponent } from './components/job-list/job-list.component';

const routes: Routes = [
  { path : '' , component : MainComponent,
      children: [
        { path: '', component: JobListComponent },
        { path: 'job/:id', component: SingleJobComponent }
      ]
  },
  { path: '**', redirectTo: '' } 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobRoutingModule { }
